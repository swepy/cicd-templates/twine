# Twine template

[![Pipeline](https://lab.frogg.it/swepy/cicd-templates/twine/badges/main/pipeline.svg)](https://lab.frogg.it/swepy/cicd-templates/twine/-/pipelines)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://lab.frogg.it/swepy/cicd-templates/twine/-/blob/main/LICENSE)

## Objective

The `twine` template is a GitLab CI/CD template that provides a job to upload a package
to a package index using `twine`.

## How to use it

1. Include the `twine` template in your CI/CD configuration (see quick use above).
2. If you need to customize the job, check
   the [jobs customization](https://docs.r2devops.io/get-started/use-templates/#job-templates-customization).

## Variables

| Name                   | Description                                                                                    | Default                                                                                                                              |
|------------------------|------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------|
| `TWINE_REPOSITORY`     | The repository (package index) to upload the package to.                                       | `pypi`                                                                                                                               |
| `TWINE_REPOSITORY_URL` | The repository (package index) URL to upload the package to. This overrides `TWINE_REPOSITORY` |                                                                                                                                      |
| `TWINE_USERNAME`       | The username to authenticate to the repository (package index) as.                             | `__token__`                                                                                                                          |
| `TWINE_PASSWORD`       | The password to authenticate to the repository (package index) with.                           |                                                                                                                                      |
| `TWINE_CERT`           | Path to alternate CA bundle.                                                                   |                                                                                                                                      |
| `TWINE_CMD`            | The twine command to run.                                                                      | `'twine upload --disable-progress-bar --password $TWINE_PASSWORD --non-interactive $DIST_DIR/*'`                                     |
| `IMAGE_NAME`           | The default name for the docker image.                                                         | `"python"`                                                                                                                           |
| `IMAGE_TAG`            | The default tag for the docker image.                                                          | `"latest"`                                                                                                                           |
| `IMAGE`                | The default docker image name.                                                                 | `"$IMAGE_NAME:$IMAGE_TAG"`                                                                                                           |
| `DIST_DIR`             | The directory containing the distribution to upload.                                           | `dist`                                                                                                                               |

