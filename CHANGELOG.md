# Changelog

All notable changes to this job will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.0] - 2024-06-10

### Fixed

* Release pipeline

## [0.1.2] - 2024-05-11

### Fixed

* Twine job image wasn't defined, causing the job to fail; it now uses
  the `python:latest` image by default

## [0.1.1] - 2024-05-11

### Changed

* Removed redundant --username options (now rely on the TWINE_USERNAME environment
  variable)
* Removed redundant --verbose options

## [0.1.0] - 2024-05-10

* Initial version
